package tn.essat.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.User;
@Repository
public interface IUserDAO extends CrudRepository<User, Integer>{
public User findByEmailAndPassword(String email,String password);
}
