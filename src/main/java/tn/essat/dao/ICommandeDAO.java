package tn.essat.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.Categorie;
import tn.essat.model.Commande;
@Repository
public interface ICommandeDAO extends CrudRepository<Commande, Integer>{

}
