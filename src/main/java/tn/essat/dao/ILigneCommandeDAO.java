package tn.essat.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tn.essat.model.LigneCommande;
@Repository
public interface ILigneCommandeDAO extends CrudRepository<LigneCommande, Integer>{

}
