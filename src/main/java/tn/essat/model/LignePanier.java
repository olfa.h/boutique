package tn.essat.model;

public class LignePanier {
private Produit prod;
private int qte;
public LignePanier() {
	super();
	// TODO Auto-generated constructor stub
}
public LignePanier(Produit prod, int qte) {
	super();
	this.prod = prod;
	this.qte = qte;
}
public Produit getProd() {
	return prod;
}
public void setProd(Produit prod) {
	this.prod = prod;
}
public int getQte() {
	return qte;
}
public void setQte(int qte) {
	this.qte = qte;
}

}
