package tn.essat.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Commande {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer num;
private String date_cde;
@ManyToOne @JoinColumn(name = "client_id")
private User Client;
@OneToMany(cascade = CascadeType.ALL,mappedBy = "cde")
private List<LigneCommande> lignes;

public void addLigne(LigneCommande ligne) {
	ligne.setCde(this);
	this.lignes.add(ligne);
}

public Commande() {
	super();
this.lignes=new ArrayList<LigneCommande>();
}

public Commande(Integer num, String date_cde, User client, List<LigneCommande> lignes) {
	super();
	this.num = num;
	this.date_cde = date_cde;
	Client = client;
	this.lignes = lignes;
}


public Integer getNum() {
	return num;
}

public void setNum(Integer num) {
	this.num = num;
}

public String getDate_cde() {
	return date_cde;
}

public void setDate_cde(String date_cde) {
	this.date_cde = date_cde;
}

public User getClient() {
	return Client;
}

public void setClient(User client) {
	Client = client;
}

public List<LigneCommande> getLignes() {
	return lignes;
}

public void setLignes(List<LigneCommande> lignes) {
	this.lignes = lignes;
}

}
