package tn.essat.model;

import java.util.ArrayList;
import java.util.List;

public class Panier {
List<LignePanier> lignes;


public void addLigne(LignePanier ligne) {
	this.lignes.add(ligne);
}
public Panier() {
	lignes= new ArrayList<LignePanier>();
	
}

public Panier(List<LignePanier> lignes) {
	super();
	this.lignes = lignes;
}

public List<LignePanier> getLignes() {
	return lignes;
}

public void setLignes(List<LignePanier> lignes) {
	this.lignes = lignes;
}

}
