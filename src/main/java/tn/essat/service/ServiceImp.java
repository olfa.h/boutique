package tn.essat.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.essat.dao.ICategorieDAO;
import tn.essat.dao.ICommandeDAO;
import tn.essat.dao.ILigneCommandeDAO;
import tn.essat.dao.IProduitDAO;
import tn.essat.dao.IUserDAO;
import tn.essat.model.Categorie;
import tn.essat.model.Commande;
import tn.essat.model.Produit;
import tn.essat.model.User;

@Service
public class ServiceImp implements IService {
	@Autowired
IProduitDAO prod;
	@Autowired
ICategorieDAO cat;
	@Autowired
IUserDAO user;
	@Autowired
ICommandeDAO cde;
	@Autowired
ILigneCommandeDAO lignecde;
	public void setUser(IUserDAO user) {
		this.user = user;
	}
	public void setProd(IProduitDAO prod) {
		this.prod = prod;
	}
	public void setCat(ICategorieDAO cat) {
		this.cat = cat;
	}
	public void setCde(ICommandeDAO cde) {
		this.cde = cde;
	}
	public void setLignecde(ILigneCommandeDAO lignecde) {
		this.lignecde = lignecde;
	}
	public List<Produit> getAllProduits() {
	
		return (List<Produit>) prod.findAll();
	}

	public List<Produit> getAllProduitsBycatId(int id) {
		// TODO Auto-generated method stub
		return (List<Produit>) prod.getAllProduitsByCatId(id);
	}

	public List<Categorie> getAllCategories() {
		// TODO Auto-generated method stub
		return (List<Categorie>) cat.findAll();
	}

	public Produit getproduitById(int id) {
		
		return prod.findOne(id);
	}

	public Commande getCommande(int id) {
		
		return cde.findOne(id);
	}

	public void addCommande(Commande cde1) {
		cde.save(cde1);
		
	}

	public User getUserbyId(int id) {
	
		return user.findOne(id);
	}

	public User verifUser(String email, String password) {
		
		return user.findByEmailAndPassword(email, password);
	}

	public void addUser(User u) {
	user.save(u);
		
	}

}
