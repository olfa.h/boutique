package tn.essat.service;

import java.util.List;

import org.springframework.stereotype.Service;

import tn.essat.model.Categorie;
import tn.essat.model.Commande;
import tn.essat.model.Produit;
import tn.essat.model.User;
@Service
public interface IService {
public List<Produit> getAllProduits();
public List<Produit> getAllProduitsBycatId(int id);
public List<Categorie> getAllCategories();
public Produit getproduitById(int id);
public Commande getCommande(int id);
public void addCommande(Commande cde);
public User getUserbyId(int id);
public User verifUser(String email,String password);
public void addUser(User u);

}
