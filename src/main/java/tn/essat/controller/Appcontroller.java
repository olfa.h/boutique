package tn.essat.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import tn.essat.model.Categorie;
import tn.essat.model.Commande;
import tn.essat.model.LigneCommande;
import tn.essat.model.LignePanier;
import tn.essat.model.Panier;
import tn.essat.model.Produit;
import tn.essat.model.User;
import tn.essat.service.IService;

@Controller
public class Appcontroller {
	@Autowired
IService service;
	public void setService(IService service) {
		this.service = service;
	}
	@GetMapping("/home")
	public String get1(Model model) {
		List<Produit> liste1=service.getAllProduits();
		List<Categorie> liste2=service.getAllCategories();
		model.addAttribute("prods",liste1);
		model.addAttribute("cats",liste2);
		return "acceuil";
				
	}
	@GetMapping("/produits/{id}")
	public String get2(Model model, @PathVariable("id") int id) {
		List<Produit> liste1=service.getAllProduitsBycatId(id);
		List<Categorie> liste2=service.getAllCategories();
		model.addAttribute("prods",liste1);
		model.addAttribute("cats",liste2);
		return "acceuil";
	}
	@GetMapping("/ajoutpanier/{id}")
	public String get3(Model model, @PathVariable("id") int id, HttpSession session) {
		
		Panier pan= (Panier) session.getAttribute("panier");
		if(pan==null) {
			Panier p = new Panier();
			Produit prod= service.getproduitById(id);
			p.addLigne(new LignePanier(prod, 1));
			session.setAttribute("panier",p);
		}else {
			boolean test=false;
			for(LignePanier li:pan.getLignes()) {
				if(li.getProd().getId()==id) {
					li.setQte(li.getQte()+1);
					test=true;
				}
			}
			if(test==false) {
				Produit prod= service.getproduitById(id);
				pan.addLigne(new LignePanier(prod, 1));
				
			}
			session.setAttribute("panier", pan);
		}
		return "redirect:/monpanier";
	}
	@GetMapping("/monpanier")
	public String get5(Model model) {
		List<Categorie> liste2=service.getAllCategories();
		model.addAttribute("cats",liste2);
		return "panier";
	}
	@GetMapping("/supprimer/{id}")
	public String get6(Model model, @PathVariable("id") int id,HttpSession session) {
		Panier pan= (Panier) session.getAttribute("panier");
		LignePanier ligne=null;
				for(LignePanier li:pan.getLignes()) {
					if(li.getProd().getId()==id) {
						ligne=li;
					}
				}
				pan.getLignes().remove(ligne);
				session.setAttribute("panier", pan);
				return "redirect:/monpanier";
	}
	@GetMapping("/inscription")
	public String get7(Model model,HttpSession session) {
		User u=(User)session.getAttribute("user");
		if(u == null) {
		User us= new User();
		model.addAttribute("u",us);
		return "inscription";
		}else {
			return "rediret:/finaliser";
		}
	}
	@PostMapping("/addUser")
		public String get8(Model model, @ModelAttribute("u") User us) {
			service.addUser(us);
			return "redirect:/finaliser";
		
	}
	@GetMapping("/verif")
	public String get9(Model model, HttpServletRequest request, HttpSession session) {
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		
		User u=service.verifUser(email, password);
		if(u==null) {
			session.setAttribute("erreur", "SVP verifier vos parametres !!!");
			session.setAttribute("type","danger");
			return "redirect:/inscription";
		}else {
			session.setAttribute("user",u);
			return "redirect:/finaliser";
		}
		
	}
	@GetMapping("/finaliser")
	public String get10(Model model) {
		List<Categorie> liste2=service.getAllCategories();
		model.addAttribute("cats",liste2);
		return "resumeCDE";
	}
	@GetMapping("/dec")
	public String get11(Model model,HttpSession session) {
		session.removeAttribute("user");
		return "redirect:/home";
	}
	@GetMapping("/valider")
	public String get12(Model model,HttpSession session) {
		Panier pan=(Panier)session.getAttribute("panier");
		User user=(User)session.getAttribute("user");
		
		Commande cde= new Commande();
		cde.setClient(user);
		cde.setDate_cde("10/11/2019");
		
		for(LignePanier p:pan.getLignes()) {
			LigneCommande li=new LigneCommande();
			li.setProd(p.getProd());
			li.setQte(p.getQte());
			li.setCde(cde);
			cde.addLigne(li);
		}
		service.addCommande(cde);
		return "redirect:/home";
	}
}
