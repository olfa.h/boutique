<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:set var="cxt" value="${pageContext.request.contextPath}"></c:set>
<spring:url value="files/bootstrap" var="bts"></spring:url>
<spring:url value="files/images" var="img"></spring:url>
<link href="${cxt}/${bts}/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">          
            <a class="navbar-brand" href="#">E-Boutique ENIG</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>
                <li><a href='#'>Promotions</a></li>
                <li><a href='#'>Ventes Flash</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right"> 
            <li><a href="${cxt}/monpanier"></a>               
                <li><a href="#"><span class="badge badge-secondary"> Mon Panier(${fn:length(sessionScope.panier.lignes)}) </span></a>   </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">
 
    <div class="row" style="margin-top: 80px;">
	
       <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active"> Tous les Produits </a>
                <c:forEach items="${cats}" var="cat">
                <a href="${cxt}/produits/${cat.id}" class="list-group-item">${cat.description }</a>
</c:forEach>
            </div>
        </div>
		
        <div class="col-md-9">
        
        <div class="panel panel-info">
                <div class="panel-heading">Mon panier </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
							<tr> <th>#</th>
								<th>D&eacute;signation de vos articles</th>
								<th>Prix UT</th>
								<th>Quantit&eacute;</th>
								<th>Total</th>
								<th></th>
							</tr>
                        </thead>
                        <tbody>
                       		 <c:forEach items="${sessionScope.panier.lignes}" var="p">
							<tr>
								<th scope="row">1</th>
								<td>
									<img src="${cxt}/${img}/${p.prod.id}.jpg" width="10%"> ${p.prod.description}
								</td>
								<td> ${p.prod.prix} DT</td>
								<td>${p.qte}</td>
								<td>${p.prod.prix * p.qte}</td>
								<td><a class="btn btn-danger " href="${cxt}/supprimer/${p.prod.id}">Suppimer </a></td>
							</tr>
							</c:forEach>
                        </tbody>						
                    </table>

                </div>
				<div class="panel-footer">					
						<a href="${cxt}/home" class="btn btn-warning">POURSUIVRE VOS ACHATS</a>
						<a href="${cxt}/inscription" class="btn btn-info">FINALISER VOTRE COMMANDE</a>				
				</class>
            </div>
        </div>
 	
        </div>

    </div>

</div>

</body>
</html>