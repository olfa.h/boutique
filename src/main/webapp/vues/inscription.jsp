<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:set var="cxt" value="${pageContext.request.contextPath}"></c:set>
<spring:url value="files/bootstrap" var="bts"></spring:url>
<spring:url value="files/images" var="img"></spring:url>
<link href="${cxt}/${bts}/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">          
            <a class="navbar-brand" href="#">E-Boutique ENIG</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>
                <li><a href='#'>Promotions</a></li>
                <li><a href='#'>Ventes Flash</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">                
                <li><a href="#"> Mon Panier() </a>   </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">

    <div class="row" style="margin-top: 80px;">
        


        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-body">
					
					
				<form class="form-horizontal" action="verif" method="get">
				 <h4 align="center">Vous Avez Déjà Un Compte E-Boutique ?<br>
  				 Connectez-Vous !
				 </h4>
				 <br>
				  <div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
					  <input type="email" class="form-control" placeholder="Email" name="email">
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-md-4 control-label">Mot de passe</label>
					<div class="col-md-8">
					  <input type="password" class="form-control" placeholder="Password" name="password">
					</div>
				  </div>
				  
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" class="btn btn-info">Se Connecter</button>
					</div>
				  </div>
				  <div class="alert alert-${sessionScope.type }">
				  ${sessionScope.erreur}</div>
				</form>
					
                </div>
            </div>
        </div>
		<div class="col-md-6" style="margin-left:-20px;">
            <div class="panel panel-info">
              <div class="panel-body">
				<form:form class="form-horizontal" method="post" action="addUser" modelAttribute="u">
				<form:input path="id" type="hidden"/>
				  <h4 align="center">Je Suis Un Nouveau Client </h4>
				  <br />
				  <h5>Créer un nouveau compte client</h5>
				  <br />				
				  <div class="form-group">
					<label class="col-md-4 control-label">Nom</label>
					<div class="col-md-8">
					  <form:input path="nom" type="text" class="form-control" placeholder="Nom" />
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-md-4 control-label">Prénom</label>
					<div class="col-md-8">
					  <form:input path ="prenom" type="text" class="form-control" placeholder="Prenom"/>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-md-4 control-label">Adresse</label>
					<div class="col-md-8">
					  <form:input path="adress" type="text" class="form-control" placeholder="Adresse"/>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
					  <form:input path="email" type="email" class="form-control" placeholder="Email"/>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-md-4 control-label">Mot de passe</label>
					<div class="col-md-8">
					  <form:input path="password" type="password" class="form-control" placeholder="Password"/>
					</div>
				  </div>
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" class="btn btn-info">S'inscrire</button>
					</div>
				  </div>
				</form:form>
					
                </div>
            </div>
        </div>
		
		
    </div>


</div>


</body>
</html>