<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<c:set var="cxt" value="${pageContext.request.contextPath}"></c:set>

<spring:url value="files/bootstrap" var="bts"></spring:url>
<spring:url value="files/images" var="img"></spring:url>

<link href="${cxt}/${bts}/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">          
            <a class="navbar-brand" href="#">E-Boutique ENIG</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>
                <li><a href='#'>Promotions</a></li>
                <li><a href='#'>Ventes Flash</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right"> 
            <li>
            <a href="${cxt}/dec">${sessionScope.user.nom} </a></li>
            <li>
            <a href="${cxt}/monpanier">   Mon panier           
                <span${fn:length(sessionScope.panier.lignes)})></span></a>  </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">
 
    <div class="row" style="margin-top: 80px;">
	
       <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active"> Tous les Produits </a>
                <c:forEach items="${cats}" var="cat">
                <a href="${cxt}/produits/${cat.id}" class="list-group-item">${cat.description }</a>
</c:forEach>
            </div>
        </div>
		
        <div class="col-md-9">
 	<c:forEach items="${prods}" var="prod">
            <div class="col-md-4">
                <div class="thumbnail">
                    <img src="${cxt}/${img}/${prod.id}.jpg" >
                    <div class="caption">
                        <h3>${prod.description}</h3>
                        <p><a href="${cxt}/ajoutpanier/${prod.id}" class="btn btn-primary" >Ajouter Panier</a>
                            <a href="#" class="btn btn-danger" >${prod.prix}DT</a>
                        </p>
                    </div>
                </div>
            </div> 
                  
          </c:forEach>
        </div>

    </div>

</div>

</body>
</html>