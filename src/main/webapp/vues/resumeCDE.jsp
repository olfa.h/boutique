<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:set var="cxt" value="${pageContext.request.contextPath}"></c:set>
<spring:url value="files/bootstrap" var="bts"></spring:url>
<spring:url value="files/images" var="img"></spring:url>
<link href="${cxt}/${bts}/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">          
            <a class="navbar-brand" href="#">E-Boutique ENIG</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>
                <li><a href='#'>Promotions</a></li>
                <li><a href='#'>Ventes Flash</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right"> 
            <li><a href="${cxt}/monpanier"></a>               
                <li><a href="#"> Mon Panier(${fn:length(sessionScope.panier.lignes)}) </a>   </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container">
 
    <div class="row" style="margin-top: 80px;">
	
       <div class="col-md-3">
            <div class="list-group">
                <a href="#" class="list-group-item active"> Tous les Produits </a>
                <c:forEach items="${cats}" var="cat">
                <a href="${cxt}/produits/${cat.id}" class="list-group-item">${cat.description }</a>
</c:forEach>
            </div>
        </div>
		
       
       
        <div class="col-md-9">
            <div class="panel panel-warning">
                <div class="panel-heading"><b>1. Adresse</b></div>
                <div class="panel-body">
                   <h5> 
				   Votre adresse est : ${sessionScope.user.adress} 
				   </h5>
				   
                </div>
				
            </div>
			
			 <div class="panel panel-info">
                <div class="panel-heading"><b>2. Mode de livraison </b></div>
                <div class="panel-body">
                    <table class="table table table-striped">
                      <tr>								
						<td>Sous Total Commande</td>
						<td> 1500 Dt</td>								
					  </tr>
					  <tr>								
						<td>Montant de livraison</td>
						<td> 10 Dt</td>								
					  </tr>
                      <tbody>
					  <tr class="success">								
						<td><b>Total</b></td>
						<td><b> 1510 DT </b></td>								
					  </tr>
					  </tbody>					   
                    </table>

                </div>
				
            </div>
			
			<div class="panel panel-primary">
                <div class="panel-heading"><b>3. Mode de paiement</b></div>
                <div class="panel-body">
                   <h5> 
                   
				   </h5>
				   
                </div>
				
            </div>
			
			<a href="#" class="btn btn-danger">Annuler</a>
			<a href="${cxt}/valider" class="btn btn-success">Valider la commande</a>
			
		
		</div>
    </div>


</div>

   

</body>
</html>